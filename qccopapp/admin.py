from django.contrib import admin

# Register your models here.

from qccopapp.models import Material, MaterialVar, MaterialStatus, Server, TrackType, Variable, Profile, ProfileVar

admin.site.register(Material)
admin.site.register(MaterialVar)
admin.site.register(MaterialStatus)
admin.site.register(Server)
admin.site.register(TrackType)
admin.site.register(Variable)
admin.site.register(Profile)
admin.site.register(ProfileVar)
