# -*- coding: utf-8 -*-

from django.db import models
from smart_selects.db_fields import ChainedForeignKey
from django.db.models.signals import post_save
from hashids import Hashids

# Create your models here.

class Server(models.Model):
    name = models.CharField(max_length=150)
    url = models.CharField(max_length=255)
    port = models.IntegerField(default=21)
    username = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    is_source = models.BooleanField(default=True)
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        verbose_name = 'Servidor FTP'
        verbose_name_plural = 'Servidores FTP'

class TrackType(models.Model):
    name = models.CharField(max_length=255)
    
    def __unicode__(self):
        return self.name

class Variable(models.Model):
    name = models.CharField(max_length=255)
    tagname = models.CharField(max_length=255)
    tracktype = models.ForeignKey(TrackType)
    
    def __unicode__(self):
        return self.name

class Profile(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    hashid = models.CharField(max_length=32, null=True, blank=True)
    
    def __unicode__(self):
        return self.name

def add_hashid(sender, instance, **kwargs):
        if instance.pk:
            if not instance.hashid:
                hashid = Hashids(min_length=8)
                instance.hashid = hashid.encrypt(instance.pk)
                print instance.hashid
                post_save.disconnect(add_hashid, sender=Profile)
                instance.save()
                post_save.connect(add_hashid, sender=Profile)

post_save.connect(add_hashid, sender=Profile)

COMPARISSON_OPERATOR_CHOICES = (
    ("<=", "menor o igual que"),
    ("=", "igual a"),
    (">=", "mayor o igual que"),
)

PROFILE_VAR_VALUE_CHOICES = (
    ("req", "Required"),
    ("inf", "Influential"),
)

class ProfileVar(models.Model):
    profile = models.ForeignKey(Profile)
    tracktype = models.ForeignKey(TrackType)
    variable = ChainedForeignKey(Variable, chained_field="tracktype", chained_model_field="tracktype")
    value = models.CharField(max_length=255)
    relationship = models.CharField(max_length=2, choices=COMPARISSON_OPERATOR_CHOICES, default="=", verbose_name="Relation")
    importance = models.CharField(max_length=3, choices=PROFILE_VAR_VALUE_CHOICES, default="REQ")
    
    
    def __unicode__(self):
        return "%s / %s %s %s" % (self.tracktype, self.variable, self.relationship, self.value)

class MaterialStatus(models.Model):
    abrv = models.CharField(max_length=3)
    description = models.CharField(max_length=200)
    
    def __unicode__(self):
        return self.abrv

class Material(models.Model):
    name = models.CharField(max_length=255)
    size = models.BigIntegerField(default=0)
    status = models.ForeignKey(MaterialStatus, default=1)
    ftp_detected = models.DateTimeField(auto_now_add=True)
    ftp_last_checked = models.DateTimeField(auto_now=True)
    ftp_start_transfer = models.DateTimeField(null=True, blank=True)
    ftp_end_transfer = models.DateTimeField(null=True, blank=True)
    server_from = models.ForeignKey(Server)
    
    def __unicode__(self):
        return self.name

class MaterialVar(models.Model):
    material = models.ForeignKey(Material)
    variable = models.ForeignKey(Variable)
    value = models.CharField(max_length=255)
    custom_value = models.CharField(max_length=255)
    track_count = models.IntegerField()
    
    def __unicode__(self):
        return "%s" % (self.variable, )

'''
class qcResult(models.Model):
    material = models.ForeignKey(Material)
    profile = models.ForeignKey(Profile)
    


'''
