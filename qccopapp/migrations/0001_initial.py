# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import smart_selects.db_fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Material',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('size', models.BigIntegerField(default=0)),
                ('status', models.CharField(default=b'NEW', max_length=3, choices=[(b'NEW', b'New in ftp'), (b'INC', b'Size updated by changes since last check'), (b'TRA', b'Transfering to QC temporal folder'), (b'QC0', b'QC ready'), (b'QC1', b'QC done')])),
                ('ftp_detected', models.DateTimeField(auto_now_add=True)),
                ('ftp_last_checked', models.DateTimeField(auto_now=True)),
                ('ftp_start_transfer', models.DateTimeField(null=True, blank=True)),
                ('ftp_end_transfer', models.DateTimeField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='MaterialVar',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=255)),
                ('custom_value', models.CharField(max_length=255)),
                ('track_count', models.IntegerField()),
                ('material', models.ForeignKey(to='qccopapp.Material')),
            ],
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('description', models.TextField(null=True, blank=True)),
                ('hashid', models.CharField(max_length=32, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='ProfileVar',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=255)),
                ('relationship', models.CharField(default=b'=', max_length=2, verbose_name=b'Relation', choices=[(b'<=', b'menor o igual que'), (b'=', b'igual a'), (b'>=', b'mayor o igual que')])),
                ('importance', models.CharField(default=b'REQ', max_length=3, choices=[(b'req', b'Required'), (b'inf', b'Influential')])),
                ('profile', models.ForeignKey(to='qccopapp.Profile')),
            ],
        ),
        migrations.CreateModel(
            name='Server',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150)),
                ('url', models.CharField(max_length=255)),
                ('port', models.IntegerField(default=21)),
                ('username', models.CharField(max_length=100)),
                ('password', models.CharField(max_length=100)),
                ('is_source', models.BooleanField(default=True)),
            ],
            options={
                'verbose_name': 'Servidor FTP',
                'verbose_name_plural': 'Servidores FTP',
            },
        ),
        migrations.CreateModel(
            name='TrackType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Variable',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('tagname', models.CharField(max_length=255)),
                ('tracktype', models.ForeignKey(to='qccopapp.TrackType')),
            ],
        ),
        migrations.AddField(
            model_name='profilevar',
            name='tracktype',
            field=models.ForeignKey(to='qccopapp.TrackType'),
        ),
        migrations.AddField(
            model_name='profilevar',
            name='variable',
            field=smart_selects.db_fields.ChainedForeignKey(chained_model_field=b'tracktype', to='qccopapp.Variable', chained_field=b'tracktype'),
        ),
        migrations.AddField(
            model_name='materialvar',
            name='variable',
            field=models.ForeignKey(to='qccopapp.Variable'),
        ),
        migrations.AddField(
            model_name='material',
            name='server_from',
            field=models.ForeignKey(to='qccopapp.Server'),
        ),
    ]
