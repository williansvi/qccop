# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('qccopapp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='MaterialStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('abrv', models.CharField(max_length=3)),
                ('description', models.CharField(max_length=200)),
            ],
        ),
    ]
