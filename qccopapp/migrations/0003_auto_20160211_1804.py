# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('qccopapp', '0002_materialstatus'),
    ]

    operations = [
        migrations.AlterField(
            model_name='material',
            name='status',
            field=models.ForeignKey(default=1, to='qccopapp.MaterialStatus'),
        ),
    ]
