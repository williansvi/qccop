# -*- coding: utf-8 -*-

from django.forms import ModelForm, HiddenInput, TextInput, ChoiceField, Select
from qccopapp.models import ProfileVar, TrackType

class AddProfileVar(ModelForm):
    
    class Meta:
        model = ProfileVar
        fields = ['profile', 'variable', 'value', 'relationship', 'importance']
        
    def __init__(self, *args, **kwargs):
    
        RELATIONSHIP_CHOICES = (('<=', '<='), ('=', '='), ('>=', '>='))
        IMPORTANCE_CHOICES = (("req", "Required"), ("inf", "Influential"))
        
        super(AddProfileVar, self).__init__(*args, **kwargs)
        self.fields['profile'].widget = HiddenInput()
        self.fields['variable'].widget = Select(attrs={'class': 'form-control'},)
        self.fields['relationship'].widget = Select(attrs={'class': 'form-control'}, choices=RELATIONSHIP_CHOICES)
        self.fields['value'].widget = TextInput(attrs={'class': 'form-control', 'placeholder': '_'})
        self.fields['importance'].widget = Select(attrs={'class': 'form-control'}, choices=IMPORTANCE_CHOICES)
