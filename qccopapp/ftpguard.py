#!/home/williansvi/.virtualenvs/django1.8.3/bin/python2
# -*- coding: utf-8 -*-

import sys
import os
import operator

# Setup environ
sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), '..'))

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "qccop.settings")
django.setup()


############  ############  ############  ############  ############  ############

from qccopapp.models import Server, Material, MaterialStatus, MaterialVar, Profile, ProfileVar, Variable
from django.db.models import Q
from django.utils import timezone
from pymediainfo import MediaInfo
import ftputil
import re
import os

regexp = re.compile(r"^\w.*\.(mov|mp4|mkv|mxf)$")
ftp_servers = Server.objects.filter(is_source=True)

############  ############  ############  ############  ############  ############

for ftp_server in ftp_servers:
    try:
        ftp_conn = ftputil.FTPHost(ftp_server.url, ftp_server.username, ftp_server.password)
    except:
        continue

    ftp_files = filter(regexp.match, ftp_conn.listdir(ftp_conn.curdir))
    for ftp_file_name in ftp_files:
        ftp_file_size = ftp_conn.path.getsize(ftp_file_name)
        try:
            registered_file = Material.objects.get(Q(name=ftp_file_name), Q(server_from=ftp_server.pk))
            if registered_file.size < ftp_file_size:
                registered_file.size = ftp_file_size
                registered_file.status = MaterialStatus.objects.get(abrv='INC')
                registered_file.save()
                print "Remote transfer in progress..."
            elif registered_file.size == ftp_file_size:
                registered_file.status = MaterialStatus.objects.get(abrv='TR0')
                registered_file.save()
                print "Remote transfer finished... Downloading to local."
        except:
            registered_file = Material(name=ftp_file_name, size=ftp_file_size, server_from=ftp_server)
            registered_file.save()
            print "Material nuevo registrado..."
    for registered_file in Material.objects.filter(Q(status=MaterialStatus.objects.get(abrv='TR0')), Q(server_from=ftp_server.pk)):
        registered_file.status = MaterialStatus.objects.get(abrv='TR1')
        registered_file.save()
        print "-> Downloading %s" % (registered_file.name, )
        ftp_conn.download(registered_file.name, os.path.join("/tmp/", registered_file.name))
        print "-> Downloaded..."
        registered_file.status = MaterialStatus.objects.get(abrv='QC0')
        registered_file.save()
        ftp_conn.remove(registered_file.name)
    ftp_conn.close()

############  ############  ############  ############  ############  ############

operators = {'<=': operator.le, '=': operator.eq, '>=': operator.ge}

for registered_file in Material.objects.filter(status=MaterialStatus.objects.get(abrv='QC0')):
    print "\nExecuting QC to %s" % (registered_file.name)
    file_mediainfo = MediaInfo.parse(os.path.join("/tmp/", registered_file.name))
    track_count = 0
    for track in file_mediainfo.tracks:
        track_count += 1
        variables = Variable.objects.filter(tracktype__name=str(track.track_type))
        for variable in variables:
            tagvalue = str(getattr(track, variable.tagname))
            if tagvalue != 'None':
                custom_tagvalue = tagvalue
                registered_file_tag = MaterialVar(material=registered_file, variable=variable, value=tagvalue, custom_value=custom_tagvalue, track_count=track_count)
                registered_file_tag.save()
    registered_file.status = MaterialStatus.objects.get(abrv='QC1')
    registered_file.save()
    
    profiles = Profile.objects.all()
    all_profile_vars = ProfileVar.objects.all()
    
    for profile in profiles:
        
        profile_vars = all_profile_vars.filter(profile=profile)
        material_vars = MaterialVar.objects.filter(material=registered_file)
        
        print "\nProfile: %s\n" % (profile, )
        
        for profile_var in profile_vars:
            for material_var in material_vars:
                if profile_var.variable == material_var.variable:
                    operation = operators[profile_var.relationship]
                    if operation(material_var.value, profile_var.value):
                        print '%s OK' % (profile_var.variable.name, )
                    else:
                        print '%s ERROR' % (profile_var.variable.name, )
        
############  ############  ############  ############  ############  ############

