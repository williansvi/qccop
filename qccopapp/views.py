# -*- coding: utf-8 -*-

from django.shortcuts import render, redirect, render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from django.core import serializers
import json
from qccopapp.models import Material, MaterialVar, MaterialStatus, Profile, ProfileVar, TrackType, Variable
from qccopapp.forms import AddProfileVar

# Create your views here.

def index(request):
    processed_mats = Material.objects.filter(status=MaterialStatus.objects.get(abrv='QC1')).order_by('-ftp_end_transfer')
    QCs = ['HD', 'SD Stereo', 'SD mono']
    return render_to_response('index.html', locals(), context_instance=RequestContext(request))

def materialslist(request):
    processed_mats = Material.objects.filter(status=MaterialStatus.objects.get(abrv='QC1')).order_by('-ftp_end_transfer')
    QCs = ['HD', 'SD Stereo', 'SD mono']
    return render_to_response('material-list.html', locals(), context_instance=RequestContext(request))



def material(request, material):
    try:
        material = Material.objects.get(pk=material)
        qcvariables = MaterialVar.objects.filter(material=material).order_by('track_count')
        profiles = Profile.objects.all()
    except:
        pass
    return render_to_response('material.html', locals(), context_instance=RequestContext(request))



def profileslist(request):
    profiles = Profile.objects.all()
    return render_to_response('profiles-list.html', locals(), context_instance=RequestContext(request))



def editprofile(request):
    
    form = AddProfileVar()
    
    if request.method == "POST":
        profile_name = request.POST.get('profile')
        try:
            profile = Profile.objects.get(name=profile_name)
        except:
            profile = Profile(name=profile_name)
            profile.save()
        return redirect("/profiles/edit/?p=%s" % profile.hashid)
    elif request.method == "GET":
        profile_hash = request.GET.get('p')
        profile = Profile.objects.get(hashid=profile_hash)
        tracktypes = TrackType.objects.all()
        profilevars = ProfileVar.objects.filter(profile=profile.pk).order_by('tracktype').order_by('variable')
    else:
        pass
    return render_to_response('profiles-edit.html', locals(), context_instance=RequestContext(request))



def sourceslist(request):
    return render_to_response('sources-list.html', locals(), context_instance=RequestContext(request))



def ajax_getpendants(request):
    data = None
    if request.is_ajax():
        pendant_materials = Material.objects.filter(status__abrv__in=['NEW', 'INC', 'TR0', 'TR1', 'QC0',])
        
        custom_pendant_materials = []
        
        for pendant_material in pendant_materials:
            
            custom_pendant_material = {}
            
            custom_pendant_material['name'] = pendant_material.name
            custom_pendant_material['status'] = pendant_material.status.description
            custom_pendant_material['server_from'] = pendant_material.server_from.url
            
            custom_pendant_materials.append(custom_pendant_material)
        
        data = json.dumps(custom_pendant_materials)
    return HttpResponse(data, content_type='application/json')



def ajax_gettags(request):
    data = None
    if request.is_ajax():
        tractype_id = request.GET.get('ttype')
        profile_hash = request.GET.get('profile')
        already_used_tags = [varid['profilevar__variable'] for varid in Profile.objects.filter(hashid=profile_hash).values('profilevar__variable')]
        if already_used_tags != [None]:
            variables = Variable.objects.filter(tracktype=tractype_id).exclude(id__in=already_used_tags).order_by('id')
        else:
            variables = Variable.objects.filter(tracktype=tractype_id).order_by('id')
        data = serializers.serialize("json", variables)
    return HttpResponse(data, content_type='application/json')



def ajax_addtag(request):
    data = None
    if request.is_ajax():
        profile_hash = request.GET.get('profile')
        profile = Profile.objects.get(hashid=profile_hash)
        track_id = request.GET.get('track')
        track = TrackType.objects.get(pk=track_id)
        tag_id = request.GET.get('tag')
        tag = Variable.objects.get(pk=tag_id)
        rel = request.GET.get('rel')
        val = request.GET.get('val')
        imp = request.GET.get('imp')
        newtag = ProfileVar(profile=profile, tracktype=track, variable=tag, relationship=rel, value=val, importance=imp)
        newtag.save()
        tags = ProfileVar.objects.filter(profile=profile).values_list('id', 'profile', 'tracktype', 'variable__name', 'value', 'relationship', 'importance').order_by('tracktype').order_by('variable')
        data = json.dumps(list(tags))
    return HttpResponse(data, content_type='application/json')



def ajax_deltag(request):
    data = None
    if request.is_ajax():
        tag_id = request.GET.get('tag')
        tag = ProfileVar.objects.get(pk=tag_id)
        tag.delete()
        profile_hash = request.GET.get('profile')
        profile = Profile.objects.get(hashid=profile_hash)
        tags = ProfileVar.objects.filter(profile=profile).values_list('id', 'profile', 'tracktype', 'variable__name', 'value', 'relationship', 'importance').order_by('tracktype').order_by('variable')
        data = json.dumps(list(tags))
    return HttpResponse(data, content_type='application/json')



def ajax_delprofile(request):
    if request.GET:
        hashid = request.GET.get('hashid')
        Profile.objects.get(hashid=hashid).delete()
    return redirect("/profiles/")
