"""qccop URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

from django.conf import settings

urlpatterns = [
    url(r'^$', 'qccopapp.views.index', name='index'),
    url(r'^material/(?P<material>[0-9]*)/$', 'qccopapp.views.material', name='material'),
    url(r'^material-list/$', 'qccopapp.views.materialslist', name='material-list'),
    url(r'^profiles/$', 'qccopapp.views.profileslist', name='profileslist'),
    url(r'^profiles/edit/$', 'qccopapp.views.editprofile', name='editprofile'),
    url(r'^sources/$', 'qccopapp.views.sourceslist', name='sourceslist'),
    url(r'^ajax/getpendants/$', 'qccopapp.views.ajax_getpendants', name='ajax-getpendants'),
    url(r'^ajax/gettags/$', 'qccopapp.views.ajax_gettags', name='ajax-gettags'),
    url(r'^ajax/delprofile/$', 'qccopapp.views.ajax_delprofile', name='ajax-delprofile'),
    url(r'^ajax/addtag/$', 'qccopapp.views.ajax_addtag', name='ajax-addtag'),
    url(r'^ajax/deltag/$', 'qccopapp.views.ajax_deltag', name='ajax-deltag'),
    url(r'^admin/', include(admin.site.urls)),
    
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',{'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    url(r'^chaining/', include('smart_selects.urls')),
]
